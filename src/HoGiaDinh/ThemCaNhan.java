package HoGiaDinh;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import model.CaNhan;
import model.NhanVien;

public class ThemCaNhan {
	static String siteUrl="http://localhost:8080/hogiadinh/form";
	WebDriver driver; 
	JavascriptExecutor js;
	static ArrayList<CaNhan> list= new ArrayList<CaNhan>();
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="CaNhan";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
				CaNhan caNhan=new CaNhan();
				caNhan.setName(row.getCell(0).getStringCellValue());
				caNhan.setNgaySinh(row.getCell(1).getStringCellValue());
				caNhan.setGioiTinh(row.getCell(2).getStringCellValue());
				caNhan.setSoCMND(row.getCell(3).getStringCellValue());
				caNhan.setDiaChi(row.getCell(4).getStringCellValue());
				caNhan.setEmail(row.getCell(5).getStringCellValue()); 
				caNhan.setSoDT(row.getCell(6).getStringCellValue());
				caNhan.setThanhPho(row.getCell(7).getStringCellValue());
				caNhan.setQuan(row.getCell(8).getStringCellValue());
				caNhan.setMaBHXH(row.getCell(9).getStringCellValue());
				caNhan.setNoiKCB(row.getCell(10).getStringCellValue());
			list.add(caNhan);
		}
		workbook.close();
		
	}
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions();
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	
	WebElement txtUsername=driver.findElement(By.name("username"));
	WebElement txtPassword= driver.findElement(By.name("password"));
	WebElement btnLogin=driver.findElement(By.className("btn-login"));
	
	
	txtUsername.sendKeys("admin");
	txtPassword.sendKeys("admin");
	btnLogin.click();
	
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000); 
		driver.close();
	}
	
	@Test
	public void test_Trung_Ma_BHXH_Ca_Nhan() {
		WebElement name =driver.findElement(By.name("name"));
		WebElement soCMND=driver.findElement(By.name("soCMND"));
		WebElement maBHXH=driver.findElement(By.name("maBHXH"));
		Select gioiTinh=new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement soDT=driver.findElement(By.name("soDT"));
		WebElement email=driver.findElement(By.name("email"));
		Select thanhPho=new Select(driver.findElement(By.id("thanhPho")));
		Select quan=new Select(driver.findElement(By.id("quan")));
		WebElement diaChi =driver.findElement(By.name("diaChi"));
		Select noiKCB =new Select(driver.findElement(By.name("noiKCB"))) ;
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd =driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		name.sendKeys(list.get(0).getName());
		soCMND.sendKeys(list.get(0).getSoCMND());
		maBHXH.sendKeys(list.get(0).getMaBHXH());
		gioiTinh.selectByVisibleText(list.get(0).getGioiTinh());
		ngaySinh.sendKeys(list.get(0).getNgaySinh());
		soDT.sendKeys(list.get(0).getSoDT());
		email.sendKeys(list.get(0).getEmail());
		thanhPho.selectByVisibleText(list.get(0).getThanhPho());
		quan.selectByVisibleText(list.get(0).getQuan());;
		diaChi.sendKeys(list.get(0).getDiaChi());
		noiKCB.selectByVisibleText(list.get(0).getNoiKCB());;
		
		btnAdd.click();
		Assertions.assertAll("Valid Add", () -> assertEquals("http://localhost:8080/hogiadinh/form?duplicate", (String) driver.getCurrentUrl()),
				() ->assertEquals("Mã Bảo hiểm xã hội đã được đăng kí", driver.findElement(By.className("alert-danger")).getText()));
	}
	
	@Test
	public void test_Trung_Ma_BHXH_Nhan_Vien() {
		WebElement name =driver.findElement(By.name("name"));
		WebElement soCMND=driver.findElement(By.name("soCMND"));
		WebElement maBHXH=driver.findElement(By.name("maBHXH"));
		Select gioiTinh=new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement soDT=driver.findElement(By.name("soDT"));
		WebElement email=driver.findElement(By.name("email"));
		Select thanhPho=new Select(driver.findElement(By.id("thanhPho")));
		Select quan=new Select(driver.findElement(By.id("quan")));
		WebElement diaChi =driver.findElement(By.name("diaChi"));
		Select noiKCB =new Select(driver.findElement(By.name("noiKCB"))) ;
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd =driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		name.sendKeys(list.get(1).getName());
		soCMND.sendKeys(list.get(1).getSoCMND());
		maBHXH.sendKeys(list.get(1).getMaBHXH());
		gioiTinh.selectByVisibleText(list.get(1).getGioiTinh());
		ngaySinh.sendKeys(list.get(1).getNgaySinh());
		soDT.sendKeys(list.get(1).getSoDT());
		email.sendKeys(list.get(1).getEmail());
		thanhPho.selectByVisibleText(list.get(1).getThanhPho());
		quan.selectByVisibleText(list.get(1).getQuan());;
		diaChi.sendKeys(list.get(1).getDiaChi());
		noiKCB.selectByVisibleText(list.get(1).getNoiKCB());;
		
		btnAdd.click();
		Assertions.assertAll("Valid Add", () -> assertEquals("http://localhost:8080/hogiadinh/form?duplicate", (String) driver.getCurrentUrl()),
				() ->assertEquals("Mã Bảo hiểm xã hội đã được đăng kí", driver.findElement(By.className("alert-danger")).getText()));
	}
}
