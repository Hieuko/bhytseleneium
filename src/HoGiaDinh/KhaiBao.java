package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import model.CaNhan;
import model.User;

class KhaiBao {
	static String siteUrl="http://localhost:8080/login";
	WebDriver driver;
	JavascriptExecutor js;
	static ArrayList<User> userlist= new ArrayList<>();
	static ArrayList<CaNhan> caNhanList = new ArrayList<>();
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="F:\\Study\\Java\\Java CB\\BHYTSelenium\\src\\data";
		String fileName="BHYT.xlsx";
		String sheetName="Khaibao";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
			
			User user=new User();
			user.setEmail(row.getCell(0).getStringCellValue());
			user.setPassword(row.getCell(1).getStringCellValue());
			userlist.add(user);
			
			CaNhan c = new CaNhan();
			c.setName(row.getCell(2).getStringCellValue());
			c.setNgaySinh(row.getCell(3).getStringCellValue());
			c.setGioiTinh(row.getCell(4).getStringCellValue());
			c.setSoCMND(row.getCell(5).getStringCellValue());
			c.setDiaChi(row.getCell(6).getStringCellValue());
			c.setEmail(row.getCell(7).getStringCellValue());
			c.setSoDT(row.getCell(8).getStringCellValue());
			c.setThanhPho(row.getCell(9).getStringCellValue());
			c.setQuan(row.getCell(10).getStringCellValue());
			c.setMaBHXH(row.getCell(11).getStringCellValue());
			c.setNoiKCB(row.getCell(12).getStringCellValue());
			caNhanList.add(c);
		}
		workbook.close();
	}
	
	@BeforeEach
	public void init() {
		ChromeOptions options=new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", "F:\\Study\\New folder\\chromedriver_win32\\chromedriver.exe");
		driver=new ChromeDriver(options);
		js=(JavascriptExecutor) driver;
		driver.get(siteUrl);
		WebElement txtUsername=driver.findElement(By.name("username"));
		WebElement txtPassword= driver.findElement(By.name("password"));
		WebElement btnLogin=driver.findElement(By.className("btn-login"));
		
		txtUsername.sendKeys(userlist.get(1).getEmail());
		txtPassword.sendKeys(userlist.get(1).getPassword());
		btnLogin.click();
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000);
		driver.close();
	}
	
	@Test
	public void test1_UI_TrangChu() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());	
	}
	
	@Test
	public void test2_UI_KhaiBaoThongTin_ChuaKhaiBao() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
	
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test3_UI_KhaiBaoThongTin_KhaiBao_ThieuHoTen() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test4_UI_KhaiBaoThongTin_KhaiBao_ThieuCMND() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear();
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test5_UI_KhaiBaoThongTin_KhaiBao_ThieuBHXH() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear();
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test6_UI_KhaiBaoThongTin_KhaiBao_SaiBHXH() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(2).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(2).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(2).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(2).getGioiTinh());
		String ngay = caNhanList.get(2).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(2).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(2).getEmail());
		tp.selectByVisibleText(caNhanList.get(2).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(2).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(2).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(2).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test7_UI_KhaiBaoThongTin_KhaiBao_ThieuNgaySinh() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		ns.clear();
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test8_UI_KhaiBaoThongTin_KhaiBao_ThieuSDT() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear();
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test9_UI_KhaiBaoThongTin_KhaiBao_SaiSDT() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(4).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(4).getEmail());
		tp.selectByVisibleText(caNhanList.get(4).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(4).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(4).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(4).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test10_UI_KhaiBaoThongTin_KhaiBao_ThieuEmail() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear();
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test11_UI_KhaiBaoThongTin_KhaiBao_SaiEmail() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(5).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(5).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(5).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(5).getGioiTinh());
		String ngay = caNhanList.get(5).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(5).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(5).getEmail());
		tp.selectByVisibleText(caNhanList.get(5).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(5).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(5).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(5).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required"));
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test12_UI_KhaiBaoThongTin_KhaiBao_ThieuThanhPho() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test13_UI_KhaiBaoThongTin_KhaiBao_ThieuQuanHuyen() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test14_UI_KhaiBaoThongTin_KhaiBao_ThieuDiaChi() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}

	@Test
	public void test15_UI_KhaiBaoThongTin_KhaiBao_KhongClickCheckBox() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		assertFalse(checkbox.isSelected());
		assertFalse(btndk.isEnabled());
	}
	
	@Test
	public void test16_UI_KhaiBaoThongTin_KhaiBao_ThanhCong() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Đăng kí", driver.getTitle());
		
		WebElement ht = driver.findElement(By.xpath("//*[@id=\"name\"]"));
		WebElement cmnd = driver.findElement(By.xpath("//*[@id=\"soCMND\"]"));
		WebElement bhxm = driver.findElement(By.xpath("//*[@id=\"maBHXH\"]"));
		Select gt = new Select(driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")));
		WebElement ns = driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]"));
		WebElement sdt = driver.findElement(By.xpath("//*[@id=\"soDT\"]"));
		WebElement email = driver.findElement(By.xpath("//*[@id=\"email\"]"));
		Select tp = new Select(driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")));
		Select qh = new Select(driver.findElement(By.xpath("//*[@id=\"quan\"]")));
		WebElement dc = driver.findElement(By.xpath("//*[@id=\"diaChi\"]"));
		Select nkcb = new Select(driver.findElement(By.xpath("//*[@id=\"noiKCB\"]")));
		WebElement checkbox = driver.findElement(By.id("check"));
		WebElement btndk = driver.findElement(By.id("btncheck"));
		WebElement btnql = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/div/form/div[6]/div/a/input"));
		
		ht.clear();  ht.sendKeys(caNhanList.get(1).getName());
		cmnd.clear(); cmnd.sendKeys(caNhanList.get(1).getSoCMND());
		bhxm.clear(); bhxm.sendKeys(caNhanList.get(1).getMaBHXH());
		gt.selectByVisibleText(caNhanList.get(1).getGioiTinh());
		String ngay = caNhanList.get(1).getNgaySinh();
		String[] s = ngay.split("-");
		String r = "";
		for (int i = s.length - 1; i >= 0; i--) r = r + s[i];
		System.out.println(r);
		ns.clear(); ns.sendKeys(r);
		sdt.clear(); sdt.sendKeys(caNhanList.get(1).getSoDT());
		email.clear(); email.sendKeys(caNhanList.get(1).getEmail());
		tp.selectByVisibleText(caNhanList.get(1).getThanhPho());
		qh.selectByVisibleText(caNhanList.get(1).getQuan());
		dc.clear(); dc.sendKeys(caNhanList.get(1).getDiaChi());
		nkcb.selectByVisibleText(caNhanList.get(1).getNoiKCB());
		if (!checkbox.isSelected()) {
			checkbox.click();
			assertTrue(btndk.isEnabled());
		}
		btndk.click();
		assertEquals("http://localhost:8080/canhan/form?success", driver.getCurrentUrl());
	}
	
	@Test
	public void test17_UI_KhaiBaoThongTin_DaKhaiBao() {
		WebElement ttcn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[1]"));
		WebElement hgd = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[2]"));
		WebElement dn = driver.findElement(By.xpath("//*[@id=\"layoutSidenav_content\"]/div/div/div/p/a[3]"));
		assertEquals("Trang chủ", driver.getTitle());
		ttcn.click();
		assertEquals("http://localhost:8080/canhan/form", driver.getCurrentUrl());
		assertEquals("Thông tin cá nhân", driver.getTitle());
		assertEquals(caNhanList.get(1).getName(), driver.findElement(By.xpath("//*[@id=\"name-profile\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getNgaySinh(), driver.findElement(By.xpath("//*[@id=\"ngaySinh\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getSoCMND(), driver.findElement(By.xpath("//*[@id=\"soCMND\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getGioiTinh(), driver.findElement(By.xpath("//*[@id=\"gioiTinh\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getEmail(), driver.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getSoDT(), driver.findElement(By.xpath("//*[@id=\"soDT\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getDiaChi(), driver.findElement(By.xpath("//*[@id=\"diaChi\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getThanhPho(), driver.findElement(By.xpath("//*[@id=\"thanhPho\"]")).getAttribute("value"));
		assertEquals(caNhanList.get(1).getQuan(), driver.findElement(By.xpath("//*[@id=\"quan\"]")).getAttribute("value"));
	}
}
