package RegisterPage;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import model.User;
public class Register {
	static String siteUrl="http://localhost:8080/registration";
	WebDriver driver;
	JavascriptExecutor js;
	static ArrayList<User> acclist= new ArrayList<User>();
	
	public static boolean isClickable(WebElement el, WebDriver driver) {
		try {
		WebDriverWait wait =new WebDriverWait(driver, 6);
		wait.until(ExpectedConditions.elementToBeClickable(el));
		return true;
		} 
		catch (Exception e){
            return false;
        }
	}
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="Register";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
			
			User user=new User();
			user.setFirstName(row.getCell(0).getStringCellValue());
			user.setLastName(row.getCell(1).getStringCellValue());
			user.setEmail(row.getCell(2).getStringCellValue());
			user.setPassword(row.getCell(3).getStringCellValue());
			user.setConfirmPassword(row.getCell(4).getStringCellValue());
			acclist.add(user);
		}
		workbook.close();
	}
	
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions();
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000);
		driver.close();
	}
	
	@Test
	public void testUI() {
		WebElement btnRegister=driver.findElement(By.id("submit"));
		WebElement Return=driver.findElement(By.xpath("/html/body/section/div/div/form/a"));
		Assertions.assertAll("Register Dispplay", () -> assertEquals("http://localhost:8080/registration", (String) driver.getCurrentUrl()),
				() -> assertEquals("Tên", driver.findElement(By.name("firstName")).getAttribute("placeholder")),
				() -> assertEquals("Họ", driver.findElement(By.name("lastName")).getAttribute("placeholder")),
				() -> assertEquals("Tên tài khoản", driver.findElement(By.name("email")).getAttribute("placeholder")),
				() -> assertEquals("Mật khẩu", driver.findElement(By.name("password")).getAttribute("placeholder")),
				() -> assertEquals("Nhập lại mật khẩu", driver.findElement(By.name("confirm_password")).getAttribute("placeholder")),
				() -> assertEquals(true, Register.isClickable(btnRegister, driver)),
				() -> assertEquals(true, Register.isClickable(Return, driver)));
	}
	
	@Test
	public void test_Return_Link() {
		WebElement Return=driver.findElement(By.xpath("/html/body/section/div/div/form/a"));
		Return.click();
		assertEquals("http://localhost:8080/login", (String) driver.getCurrentUrl());
	}
	
	
//	@Test
//	public void Valid_Register() {
//		WebElement txtFirsname=driver.findElement(By.name("firstName"));
//		WebElement txtLastname=driver.findElement(By.name("lastName"));
//		WebElement txtUsername=driver.findElement(By.name("email"));
//		WebElement txtPassword=driver.findElement(By.name("password"));
//		WebElement txtConfirmPass=driver.findElement(By.name("confirm_password"));
//		WebElement btnRegister=driver.findElement(By.id("submit"));
//		
//		
//		txtFirsname.sendKeys(acclist.get(0).getFirstName());
//		txtLastname.sendKeys(acclist.get(0).getFirstName());
//		txtUsername.sendKeys(acclist.get(0).getEmail());
//		txtPassword.sendKeys(acclist.get(0).getPassword());
//		txtConfirmPass.sendKeys(acclist.get(0).getConfirmPassword());
//		
//		btnRegister.click();
//		WebElement notify=driver.findElement(By.className("alert-success"));
//		assertEquals("Đăng kí thành công, Đăng nhập", notify.getText());
//	}
	
	@Test
	public void test_Trung_Ten_Dang_Nhap() {
		WebElement txtFirsname=driver.findElement(By.name("firstName"));
		WebElement txtLastname=driver.findElement(By.name("lastName"));
		WebElement txtUsername=driver.findElement(By.name("email"));
		WebElement txtPassword=driver.findElement(By.name("password"));
		WebElement txtConfirmPass=driver.findElement(By.name("confirm_password"));
		WebElement btnRegister=driver.findElement(By.id("submit"));
		
		
		txtFirsname.sendKeys(acclist.get(1).getFirstName());
		txtLastname.sendKeys(acclist.get(1).getFirstName());
		txtUsername.sendKeys(acclist.get(1).getEmail());
		txtPassword.sendKeys(acclist.get(1).getPassword());
		txtConfirmPass.sendKeys(acclist.get(1).getConfirmPassword());
		
		btnRegister.click();
		WebElement notify=driver.findElement(By.className("alert-danger"));
		assertEquals("Tên đăng nhập đã tồn tại", notify.getText());
	
	}
	
	@Test
	public void test_Mat_Khau_Nhap_Lai_Khong_Khop() {
		WebElement txtFirsname=driver.findElement(By.name("firstName"));
		WebElement txtLastname=driver.findElement(By.name("lastName"));
		WebElement txtUsername=driver.findElement(By.name("email"));
		WebElement txtPassword=driver.findElement(By.name("password"));
		WebElement txtConfirmPass=driver.findElement(By.name("confirm_password"));
		WebElement btnRegister=driver.findElement(By.id("submit"));
		
		
		txtFirsname.sendKeys(acclist.get(2).getFirstName());
		txtLastname.sendKeys(acclist.get(2).getFirstName());
		txtUsername.sendKeys(acclist.get(2).getEmail());
		txtPassword.sendKeys(acclist.get(2).getPassword());
		txtConfirmPass.sendKeys(acclist.get(2).getConfirmPassword());
		
		btnRegister.click();
		WebElement notify=driver.findElement(By.id("message"));
		assertEquals("Mật khẩu nhập lại không khớp", notify.getText());
	}
	
	@Test
	public void test_Khong_Nhap_Ten_Dang_Nhap() {
		WebElement txtFirsname=driver.findElement(By.name("firstName"));
		WebElement txtLastname=driver.findElement(By.name("lastName"));
		WebElement txtPassword=driver.findElement(By.name("password"));
		WebElement txtConfirmPass=driver.findElement(By.name("confirm_password"));
		WebElement btnRegister=driver.findElement(By.id("submit"));
		
		
		txtFirsname.sendKeys(acclist.get(1).getFirstName());
		txtLastname.sendKeys(acclist.get(1).getFirstName());
		txtPassword.sendKeys(acclist.get(1).getPassword());
		txtConfirmPass.sendKeys(acclist.get(1).getConfirmPassword());
		
		btnRegister.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
	
	@Test
	public void test_Khong_Nhap_Mat_Khau() {
		WebElement txtFirsname=driver.findElement(By.name("firstName"));
		WebElement txtLastname=driver.findElement(By.name("lastName"));
		WebElement txtUsername=driver.findElement(By.name("email"));
		WebElement txtConfirmPass=driver.findElement(By.name("confirm_password"));
		WebElement btnRegister=driver.findElement(By.id("submit"));
		
		
		txtFirsname.sendKeys(acclist.get(1).getFirstName());
		txtLastname.sendKeys(acclist.get(1).getFirstName());
		txtUsername.sendKeys(acclist.get(1).getEmail());
		txtConfirmPass.sendKeys(acclist.get(1).getConfirmPassword());
		
		btnRegister.click();
		WebElement elem1 = driver.findElement(By.cssSelector("input:required")); // thông báo "Vui lòng điền vào trường này của input"
		assertEquals(true, elem1.isDisplayed());
	}
}
