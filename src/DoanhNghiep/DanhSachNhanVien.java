package DoanhNghiep;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import LoginPage.Login;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.DoanhNghiep;
import model.NhanVien;

public class DanhSachNhanVien {
	static String siteUrl="http://localhost:8080/doanhnghiep";
	WebDriver driver; 
	JavascriptExecutor js;
	static ArrayList<NhanVien> list = new ArrayList<NhanVien>();
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="NhanVien";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
			
			NhanVien nhanVien =new NhanVien();
			nhanVien.setTen(row.getCell(0).getStringCellValue());
			nhanVien.setGioiTinh(row.getCell(1).getStringCellValue());
			nhanVien.setNgaySinh(row.getCell(2).getStringCellValue());
			nhanVien.setDiaChi(row.getCell(3).getStringCellValue());
			nhanVien.setMaBHXH(row.getCell(4).getStringCellValue());
			nhanVien.setChucVu(row.getCell(5).getStringCellValue());
			nhanVien.setMucluong(row.getCell(6).getStringCellValue());
			nhanVien.setPhuCap(row.getCell(7).getStringCellValue());
			nhanVien.setNoiKCB(row.getCell(8).getStringCellValue());
			
			list.add(nhanVien);
		}
		workbook.close();
	}
	
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions();
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	
	WebElement txtUsername=driver.findElement(By.name("username"));
	WebElement txtPassword= driver.findElement(By.name("password"));
	WebElement btnLogin=driver.findElement(By.className("btn-login"));
	
	
	txtUsername.sendKeys("admin");
	txtPassword.sendKeys("admin");
	btnLogin.click();
	
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000); 
		driver.close();
	}
	
	@Test
	public void testUI() { // ktra xem các nút có click đc ko
		WebElement btnAdd= driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[1]/button"));
		WebElement btnReturn=driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[2]/button"));
		WebElement btnPayment=driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[3]/button"));
		
		Assertions.assertAll("Button Display", () -> assertEquals(true, Login.isClickable(btnAdd, driver)),
				() -> assertEquals(true, Login.isClickable(btnReturn, driver)),
				() -> assertEquals(true, Login.isClickable(btnPayment, driver)));
	}
	
	@Test
	public void test_Button_Add_Nhan_Vien() { // test nut add co chuyen giao dien ko
		WebElement btnAdd= driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[1]/button"));
		btnAdd.click();
		assertEquals("http://localhost:8080/nhanvien", (String) driver.getCurrentUrl());
	}
	 
	@Test
	public void test_Button_Return() {
		WebElement btnReturn=driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[2]/button"));
		btnReturn.click();
		assertEquals("http://localhost:8080/", (String) driver.getCurrentUrl()); // về giao diện trang chủ
	}
	
	
}
