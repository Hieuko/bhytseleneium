package DoanhNghiep;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import model.NhanVien;

public class ThemNhanVien {
	static String siteUrl="http://localhost:8080/nhanvien";
	WebDriver driver; 
	JavascriptExecutor js;
	static ArrayList<NhanVien> list = new ArrayList<NhanVien>();
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="NhanVien";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
			
			NhanVien nhanVien =new NhanVien();
			nhanVien.setTen(row.getCell(0).getStringCellValue());
			nhanVien.setGioiTinh(row.getCell(1).getStringCellValue());
			nhanVien.setNgaySinh(row.getCell(2).getStringCellValue());
			nhanVien.setDiaChi(row.getCell(3).getStringCellValue());
			nhanVien.setMaBHXH(row.getCell(4).getStringCellValue());
			nhanVien.setChucVu(row.getCell(5).getStringCellValue());
			nhanVien.setMucluong(row.getCell(6).getStringCellValue());
			nhanVien.setPhuCap(row.getCell(7).getStringCellValue());
			nhanVien.setNoiKCB(row.getCell(8).getStringCellValue());
			
			list.add(nhanVien);
		}
		workbook.close();
		
	}
	
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions(); 
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	
	WebElement txtUsername=driver.findElement(By.name("username"));
	WebElement txtPassword= driver.findElement(By.name("password"));
	WebElement btnLogin=driver.findElement(By.className("btn-login"));
	
	
	txtUsername.sendKeys("admin");
	txtPassword.sendKeys("admin");
	btnLogin.click();
	
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000); 
		driver.close();
	}
	 
	@Test
	public void test_Null_Input() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		ten.sendKeys(list.get(0).getTen());
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		//gioiTinh.selectByVisibleText(list.get(0).getGioiTinh());
		ngaySinh.sendKeys(list.get(0).getNgaySinh());
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		diaChi.sendKeys(list.get(0).getDiaChi());
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		maBHXH.sendKeys(list.get(0).getMaBHXH());
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		chucVu.sendKeys(list.get(0).getChucVu());
		
		btnAdd.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		mucLuong.clear();
		mucLuong.sendKeys(list.get(0).getMucluong());
		
		//btnAdd.click();
//		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
//		phuCap.sendKeys(String.valueOf(list.get(0).getPhuCap()));
//		
		
	}
	
	@Test
	public void test_Add_Nhan_Vien_Valid() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(0).getTen());
		gioiTinh.selectByVisibleText(list.get(0).getGioiTinh());
		ngaySinh.sendKeys(list.get(0).getNgaySinh());
		diaChi.sendKeys(list.get(0).getDiaChi());
		maBHXH.sendKeys(list.get(0).getMaBHXH());
		chucVu.sendKeys(list.get(0).getChucVu());
		mucLuong.sendKeys(list.get(0).getMucluong());
		phuCap.sendKeys(list.get(0).getPhuCap());
		noiKCB.selectByVisibleText(list.get(0).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("Valid Add", () -> assertEquals("http://localhost:8080/doanhnghiep?success", (String) driver.getCurrentUrl()),
				() ->assertEquals("Bạn đã đăng kí thành công", driver.findElement(By.className("alert-info")).getText()));
		
		WebElement btnDelete=driver.findElement(By.className("btn-danger"));
		btnDelete.click();
		
	}
	
	@Test
	public void test_Trung_Ma_BHXH() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(1).getTen());
		gioiTinh.selectByVisibleText(list.get(1).getGioiTinh());
		ngaySinh.sendKeys(list.get(1).getNgaySinh());
		diaChi.sendKeys(list.get(1).getDiaChi());
		maBHXH.sendKeys(list.get(1).getMaBHXH());
		chucVu.sendKeys(list.get(1).getChucVu());
		mucLuong.sendKeys(list.get(1).getMucluong());
		phuCap.sendKeys(list.get(1).getPhuCap());
		noiKCB.selectByVisibleText(list.get(1).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add", () -> assertEquals("http://localhost:8080/nhanvien?duplicate", (String) driver.getCurrentUrl()),
				() ->assertEquals("Mã Bảo hiểm xã hội đã được đăng kí", driver.findElement(By.className("alert-danger")).getText()));
	}
	
	@Test
	public void test_Ma_BHXH_Khong_Hop_Le() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(2).getTen());
		gioiTinh.selectByVisibleText(list.get(2).getGioiTinh());
		ngaySinh.sendKeys(list.get(2).getNgaySinh());
		diaChi.sendKeys(list.get(2).getDiaChi());
		maBHXH.sendKeys(list.get(2).getMaBHXH());
		chucVu.sendKeys(list.get(2).getChucVu());
		mucLuong.sendKeys(list.get(2).getMucluong());
		phuCap.sendKeys(list.get(2).getPhuCap());
		noiKCB.selectByVisibleText(list.get(2).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add", () -> assertEquals("http://localhost:8080/nhanvien", (String) driver.getCurrentUrl()),
				() ->assertEquals("Mã BHXH có dạng 10 chữ số từ 0-9", maBHXH.getAttribute("validationMessage")));
	}
	
	@Test
	public void test_Muc_Luong_Nho_Hon_0() { //Passed nhưng chưa ktra đc thông báo lỗi
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(3).getTen());
		gioiTinh.selectByVisibleText(list.get(3).getGioiTinh());
		ngaySinh.sendKeys(list.get(3).getNgaySinh());
		diaChi.sendKeys(list.get(3).getDiaChi());
		maBHXH.sendKeys(list.get(3).getMaBHXH());
		chucVu.sendKeys(list.get(3).getChucVu());
		mucLuong.sendKeys(list.get(3).getMucluong());
		phuCap.sendKeys(list.get(3).getPhuCap());
		noiKCB.selectByVisibleText(list.get(3).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add2", () -> assertEquals("http://localhost:8080/nhanvien", (String) driver.getCurrentUrl())
				,() ->assertEquals("Giá trị phải lớn hơn hoặc bằng 1.", mucLuong.getAttribute("validationMessage"))
				);
	}
	
	@Test
	public void test_Phu_Cap_Nho_Hon_0() { //Passed nhưng chưa ktra đc thông báo lỗi
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(4).getTen());
		gioiTinh.selectByVisibleText(list.get(4).getGioiTinh());
		ngaySinh.sendKeys(list.get(4).getNgaySinh());
		diaChi.sendKeys(list.get(4).getDiaChi());
		maBHXH.sendKeys(list.get(4).getMaBHXH());
		chucVu.sendKeys(list.get(4).getChucVu());
		mucLuong.sendKeys(list.get(4).getMucluong());
		phuCap.sendKeys(list.get(4).getPhuCap());
		noiKCB.selectByVisibleText(list.get(4).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add2", () -> assertEquals("http://localhost:8080/nhanvien", (String) driver.getCurrentUrl())
				,() ->assertEquals("Giá trị phải lớn hơn hoặc bằng 0.", phuCap.getAttribute("validationMessage"))
				);
	}
	 
	@Test
	public void test_Muc_Luong_Nho_Hon_Luong_Toi_Thieu_Vung1() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(5).getTen());
		gioiTinh.selectByVisibleText(list.get(5).getGioiTinh());
		ngaySinh.sendKeys(list.get(5).getNgaySinh());
		diaChi.sendKeys(list.get(5).getDiaChi()); 
		maBHXH.sendKeys(list.get(5).getMaBHXH());
		chucVu.sendKeys(list.get(5).getChucVu());
		mucLuong.clear();
		mucLuong.sendKeys(list.get(5).getMucluong());
		phuCap.sendKeys(list.get(5).getPhuCap());
		noiKCB.selectByVisibleText(list.get(5).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add2", () -> assertEquals("http://localhost:8080/nhanvien?error=01", (String) driver.getCurrentUrl())
				,() ->assertEquals("Mức lương tối thiểu cho nhân viên của doanh nghiệp thuộc Vùng 1 là 4.729.400 VNĐ", driver.findElement(By.className("alert-danger")).getText())
				);
	}
	
	@Test
	public void test_Muc_Luong_Bang_0() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(6).getTen());
		gioiTinh.selectByVisibleText(list.get(6).getGioiTinh());
		ngaySinh.sendKeys(list.get(6).getNgaySinh());
		diaChi.sendKeys(list.get(6).getDiaChi());
		maBHXH.sendKeys(list.get(6).getMaBHXH());
		chucVu.sendKeys(list.get(6).getChucVu());
		mucLuong.clear();
		mucLuong.sendKeys(list.get(6).getMucluong());
		phuCap.clear();
		phuCap.sendKeys(list.get(6).getPhuCap());
		noiKCB.selectByVisibleText(list.get(6).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("InValid Add2", () -> assertEquals("http://localhost:8080/nhanvien", (String) driver.getCurrentUrl())
				,() ->assertEquals("Giá trị phải lớn hơn hoặc bằng 1.", mucLuong.getAttribute("validationMessage"))
				);
	}
	
	@Test
	public void test_Phu_Cap_Bang_0() {
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnAdd=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(7).getTen());
		gioiTinh.selectByVisibleText(list.get(7).getGioiTinh());
		ngaySinh.sendKeys(list.get(7).getNgaySinh());
		diaChi.sendKeys(list.get(7).getDiaChi());
		maBHXH.sendKeys(list.get(7).getMaBHXH());
		chucVu.sendKeys(list.get(7).getChucVu());
		mucLuong.clear();
		mucLuong.sendKeys(list.get(7).getMucluong());
		phuCap.clear();
		phuCap.sendKeys(list.get(7).getPhuCap());
		noiKCB.selectByVisibleText(list.get(7).getNoiKCB());
		
		btnAdd.click();
		Assertions.assertAll("Valid Add", () -> assertEquals("http://localhost:8080/doanhnghiep?success", (String) driver.getCurrentUrl()),
				() ->assertEquals("Bạn đã đăng kí thành công", driver.findElement(By.className("alert-info")).getText()));
		
		WebElement btnDelete=driver.findElement(By.className("btn-danger"));
		btnDelete.click();
	}
}
