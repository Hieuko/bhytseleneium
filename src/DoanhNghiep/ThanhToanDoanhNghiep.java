package DoanhNghiep;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import model.NhanVien;
public class ThanhToanDoanhNghiep {
	static String siteUrl="http://localhost:8080/doanhnghiep";
	WebDriver driver; 
	JavascriptExecutor js;
	static ArrayList<NhanVien> list = new ArrayList<NhanVien>();
	
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="NhanVien";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i);
			
			NhanVien nhanVien =new NhanVien();
			nhanVien.setTen(row.getCell(0).getStringCellValue());
			nhanVien.setGioiTinh(row.getCell(1).getStringCellValue());
			nhanVien.setNgaySinh(row.getCell(2).getStringCellValue());
			nhanVien.setDiaChi(row.getCell(3).getStringCellValue());
			nhanVien.setMaBHXH(row.getCell(4).getStringCellValue());
			nhanVien.setChucVu(row.getCell(5).getStringCellValue());
			nhanVien.setMucluong(row.getCell(6).getStringCellValue());
			nhanVien.setPhuCap(row.getCell(7).getStringCellValue());
			nhanVien.setNoiKCB(row.getCell(8).getStringCellValue());
			
			list.add(nhanVien);
		}
		workbook.close();
		
	}
	
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions();
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	
	WebElement txtUsername=driver.findElement(By.name("username"));
	WebElement txtPassword= driver.findElement(By.name("password"));
	WebElement btnLogin=driver.findElement(By.className("btn-login"));
	
	
	txtUsername.sendKeys("admin");
	txtPassword.sendKeys("admin");
	btnLogin.click();
	
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000); 
		driver.close();
	}
	
	@Test
	public void test_Button_Payment_Valid() { // trường hợp danh sach nhân viên trống
		WebElement total =driver.findElement(By.id("total"));
		WebElement btnPayment=driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[3]/button"));
		btnPayment.click();
		
		Assertions.assertAll("Invalid",
				() -> assertEquals("http://localhost:8080/doanhnghiep?error", (String) driver.getCurrentUrl()), // url báo lỗi
				() -> assertEquals("Chưa có danh sách nhân viên", driver.findElement(By.className("alert-danger")).getText())); // thông báo lỗi
		
	}	
	
	@Test
	public void test_Valid_Payment() {
		WebElement btnAdd= driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[1]/button"));
		
		btnAdd.click();
		
		WebElement ten=driver.findElement(By.name("ten"));
		Select gioiTinh= new Select(driver.findElement(By.name("gioiTinh")));
		WebElement ngaySinh= driver.findElement(By.name("ngaySinh"));
		WebElement diaChi= driver.findElement(By.name("diaChi"));
		WebElement maBHXH= driver.findElement(By.name("maBHXH"));
		WebElement chucVu= driver.findElement(By.name("chucVu"));
		WebElement mucLuong= driver.findElement(By.id("mucluong-input"));
		WebElement phuCap= driver.findElement(By.name("phuCap"));
		Select noiKCB= new Select(driver.findElement(By.name("noiKCB")));
		WebElement checkBox= driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		checkBox.click();
		
		ten.sendKeys(list.get(8).getTen());
		gioiTinh.selectByVisibleText(list.get(8).getGioiTinh());
		ngaySinh.sendKeys(list.get(8).getNgaySinh());
		diaChi.sendKeys(list.get(8).getDiaChi());
		maBHXH.sendKeys(list.get(8).getMaBHXH());
		chucVu.sendKeys(list.get(8).getChucVu());
		mucLuong.sendKeys(list.get(8).getMucluong());
		phuCap.sendKeys(list.get(8).getPhuCap());
		noiKCB.selectByVisibleText(list.get(8).getNoiKCB());
		
		btnRegister.click();
		Assertions.assertAll("Valid Add", () -> assertEquals("http://localhost:8080/doanhnghiep?success", (String) driver.getCurrentUrl()),
				() ->assertEquals("Bạn đã đăng kí thành công", driver.findElement(By.className("alert-info")).getText()));
		WebElement btnPayment=driver.findElement(By.xpath("/html/body/div/div[2]/div/div/div/div/a[3]/button"));
		btnPayment.click();
		Assertions.assertAll("Valid payment", () -> assertEquals("http://localhost:8080/thanhtoan/doanhnghiep/vnpay", (String) driver.getCurrentUrl())
			);

		
		
	}
}
