package DoanhNghiep;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import model.DoanhNghiep;
import model.User;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
public class DangKiDoanhNghiep {
	static String siteUrl="http://localhost:8080/doanhnghiep";
	WebDriver driver; 
	JavascriptExecutor js;
	//static ArrayList<User> acclist= new ArrayList<User>();
	static ArrayList<DoanhNghiep> list = new ArrayList<DoanhNghiep>();
	@BeforeAll
	public static void setup() throws IOException {
		String filePath="C:\\Users\\ADMINS\\eclipse-workspace\\BHYTtestSelenium\\data";
		String fileName="AccountBHYT.xlsx";
		String sheetName="DoanhNghiep";
		
		File file =new File(filePath+"\\"+fileName);
		FileInputStream inputStream=new FileInputStream(file);
		Workbook workbook= new XSSFWorkbook(inputStream);
		Sheet sheet=workbook.getSheet(sheetName);
		int rowCount =sheet.getLastRowNum()- sheet.getFirstRowNum();
		for(int i=1;i < rowCount + 1;i++) {
			Row row=sheet.getRow(i); 
			
			DoanhNghiep doanhNghiep=new DoanhNghiep();
			doanhNghiep.setTen(row.getCell(0).getStringCellValue());
			doanhNghiep.setMaDonVi(row.getCell(1).getStringCellValue());
			doanhNghiep.setMaThue(row.getCell(2).getStringCellValue());
			doanhNghiep.setDiaChiKinhDoanh(row.getCell(3).getStringCellValue());
			doanhNghiep.setDiaChiLienHe(row.getCell(4).getStringCellValue());
			doanhNghiep.setLoaiHinhKinhDoanh(row.getCell(5).getStringCellValue());
			doanhNghiep.setSoDT(row.getCell(6).getStringCellValue());
			doanhNghiep.setEmail(row.getCell(7).getStringCellValue());
			doanhNghiep.setGiayPhepKinhDoanh(row.getCell(8).getStringCellValue());
			doanhNghiep.setNoiCap(row.getCell(9).getStringCellValue());
			doanhNghiep.setPhuongThucDong(row.getCell(10).getStringCellValue());
			
			list.add(doanhNghiep);
		}
		workbook.close();
	}
	
	@BeforeEach
	public void init() {
	ChromeOptions options=new ChromeOptions();
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\ADMINS\\Desktop\\Study\\chromedriver_win32\\chromedriver.exe");
	driver=new ChromeDriver(options);
	js=(JavascriptExecutor) driver;
	driver.get(siteUrl);
	
	WebElement txtUsername=driver.findElement(By.name("username"));
	WebElement txtPassword= driver.findElement(By.name("password"));
	WebElement btnLogin=driver.findElement(By.className("btn-login"));
	
	
	txtUsername.sendKeys("admin"); 
	txtPassword.sendKeys("admin");
	btnLogin.click();
	
	}
	
	@AfterEach
	public void afterTest() throws Exception {
		Thread.sleep(1000); 
		driver.close();
	}
	
	@Test
	public void test_Dang_Ki_Thanh_Cong() {
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(0).getTen());
		maDonVi.sendKeys(list.get(0).getMaDonVi());
		maThue.sendKeys(list.get(0).getMaThue());
		loaiKD.selectByVisibleText(list.get(0).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(0).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(0).getDiaChiLienHe());
		soDT.sendKeys(list.get(0).getSoDT());
		email.sendKeys(list.get(0).getEmail());
		giayPhep.sendKeys(list.get(0).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(0).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(0).getPhuongThucDong());;
		checkBox.click();
		btnRegister.click();
		
		WebElement header= driver.findElement(By.xpath("/html/body/div/div[2]/div/h3"));
		assertEquals("Danh sách nhân viên", header.getText());
		
	}
	
	@Test
	public void check_Null_Input() { // ktra khi từng input bị để trống -> yêu cầu nhập
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
//		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
//		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
//		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		checkBox.click();
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		name.sendKeys(list.get(0).getTen()); 
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		maDonVi.sendKeys(list.get(0).getMaDonVi());
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		maThue.sendKeys(list.get(0).getMaThue());
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		diachiKD.sendKeys(list.get(0).getDiaChiKinhDoanh());
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		diachiLH.sendKeys(list.get(0).getDiaChiLienHe()); 
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		soDT.sendKeys(list.get(0).getSoDT());
		
		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		email.sendKeys(list.get(0).getEmail());

		btnRegister.click();
		assertEquals(true, driver.findElement(By.cssSelector("input:required")).isDisplayed());
		giayPhep.sendKeys(list.get(0).getGiayPhepKinhDoanh());
		
	}
	
	@Test
	public void test_So_DT_Ko_Hop_Le() {
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(1).getTen());
		maDonVi.sendKeys(list.get(1).getMaDonVi());
		maThue.sendKeys(list.get(1).getMaThue());
		loaiKD.selectByVisibleText(list.get(1).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(1).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(1).getDiaChiLienHe());
		soDT.sendKeys(list.get(1).getSoDT());
		email.sendKeys(list.get(1).getEmail());
		giayPhep.sendKeys(list.get(1).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(1).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(1).getPhuongThucDong());;
		checkBox.click();
		btnRegister.click();
		
		//WebElement notify = driver.findElement(By.cssSelector("input:required"));
		assertEquals("Số điện thoại có dạng 10 chữ số từ 0-9", soDT.getAttribute("validationMessage"));
	}
	
	@Test
	public void test_Email_Khong_Hop_Le() {
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(2).getTen());
		maDonVi.sendKeys(list.get(2).getMaDonVi());
		maThue.sendKeys(list.get(2).getMaThue());
		loaiKD.selectByVisibleText(list.get(2).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(2).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(2).getDiaChiLienHe());
		soDT.sendKeys(list.get(2).getSoDT());
		email.sendKeys(list.get(2).getEmail());
		giayPhep.sendKeys(list.get(2).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(2).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(2).getPhuongThucDong());;
		checkBox.click();
		btnRegister.click();
		
		//WebElement notify = driver.findElement(By.cssSelector("input:required"));
		assertEquals("Định dạng đúng: abc@gmail.com", email.getAttribute("validationMessage"));
	}
	
	@Test
	public void test_Ma_Doanh_Nghiep_Da_Duoc_DKi() { //Failed
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(3).getTen());
		maDonVi.sendKeys(list.get(3).getMaDonVi());
		maThue.sendKeys(list.get(3).getMaThue());
		loaiKD.selectByVisibleText(list.get(3).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(3).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(3).getDiaChiLienHe());
		soDT.sendKeys(list.get(3).getSoDT());
		email.sendKeys(list.get(3).getEmail());
		giayPhep.sendKeys(list.get(3).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(3).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(3).getPhuongThucDong());;
		checkBox.click(); 
		btnRegister.click();
		
		//check xem có ở trang đki ko
		Assertions.assertAll("Invalid Code", () -> assertEquals("Khai báo thông tin doanh nghiệp", driver.findElement(By.xpath("/html/body/div/div[2]/div/h3")).getText()),
				() -> assertEquals("Thông tin đăng kí bị trùng lặp", driver.findElement(By.className("alert-danger")).getText())); // hiển thị thông báo lỗi
	}
	
	@Test
	public void test_Ma_Thue_Da_Duoc_Dang_Ki() { //Failed
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(4).getTen());
		maDonVi.sendKeys(list.get(4).getMaDonVi());
		maThue.sendKeys(list.get(4).getMaThue());
		loaiKD.selectByVisibleText(list.get(4).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(4).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(4).getDiaChiLienHe());
		soDT.sendKeys(list.get(4).getSoDT());
		email.sendKeys(list.get(4).getEmail());
		giayPhep.sendKeys(list.get(4).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(4).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(4).getPhuongThucDong());;
		checkBox.click();
		btnRegister.click();
		
		//check xem có ở trang đki ko
		Assertions.assertAll("Invalid Code", () -> assertEquals("Khai báo thông tin doanh nghiệp", driver.findElement(By.xpath("/html/body/div/div[2]/div/h3")).getText()),
				() -> assertEquals("Thông tin đăng kí bị trùng lặp", driver.findElement(By.className("alert-danger")).getText())); // hiển thị thông báo lỗi
	}
	
	@Test
	public void test_Giay_Phep_Kinh_Doanh_Da_Dc_DKi() { // Failed
		WebElement name= driver.findElement(By.name("ten"));
		WebElement maDonVi= driver.findElement(By.name("maDonVi"));
		WebElement maThue= driver.findElement(By.name("maThue"));
		Select loaiKD=new Select(driver.findElement(By.name("loaiHinhKinhDoanh"))) ;
		WebElement diachiKD= driver.findElement(By.name("diaChiKinhDoanh"));
		WebElement diachiLH= driver.findElement(By.name("diaChiLienHe"));
		WebElement soDT= driver.findElement(By.name("soDT"));
		WebElement email= driver.findElement(By.name("email"));
		WebElement giayPhep= driver.findElement(By.name("giayPhepKinhDoanh"));
		Select noiCap=new Select(driver.findElement(By.name("noiCap"))) ;
		Select phuongThucDong= new Select(driver.findElement(By.name("phuongThucDong")));
		WebElement checkBox=driver.findElement(By.id("check"));
		WebElement btnRegister=driver.findElement(By.id("btncheck"));
		
		name.sendKeys(list.get(5).getTen());
		maDonVi.sendKeys(list.get(5).getMaDonVi());
		maThue.sendKeys(list.get(5).getMaThue());
		loaiKD.selectByVisibleText(list.get(5).getLoaiHinhKinhDoanh());;
		diachiKD.sendKeys(list.get(5).getDiaChiKinhDoanh());
		diachiLH.sendKeys(list.get(5).getDiaChiLienHe());
		soDT.sendKeys(list.get(5).getSoDT());
		email.sendKeys(list.get(5).getEmail());
		giayPhep.sendKeys(list.get(5).getGiayPhepKinhDoanh());
		noiCap.selectByVisibleText(list.get(5).getNoiCap());;
		phuongThucDong.selectByVisibleText(list.get(5).getPhuongThucDong());;
		checkBox.click();
		btnRegister.click(); 
		
		//check xem có ở trang đki ko
		Assertions.assertAll("Invalid Code", () -> assertEquals("Khai báo thông tin doanh nghiệp", driver.findElement(By.xpath("/html/body/div/div[2]/div/h3")).getText()),
				() -> assertEquals("Thông tin đăng kí bị trùng lặp", driver.findElement(By.className("alert-danger")).getText())); // hiển thị thông báo lỗi
	}
}

