package model;

public class DoanhNghiep {
	private String ten;
	private String maDonVi;
	private String maThue;
	private String diaChiKinhDoanh;
	private String diaChiLienHe;
	private String loaiHinhKinhDoanh;
	private String soDT;
	private String email;
	private String giayPhepKinhDoanh;
	private String noiCap;
	private String phuongThucDong;
	public DoanhNghiep() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DoanhNghiep(String ten, String maDonVi, String maThue, String diaChiKinhDoanh, String diaChiLienHe,
			String loaiHinhKinhDoanh, String soDT, String email, String giayPhepKinhDoanh, String noiCap,
			String phuongThucDong) {
		super();
		this.ten = ten;
		this.maDonVi = maDonVi;
		this.maThue = maThue;
		this.diaChiKinhDoanh = diaChiKinhDoanh;
		this.diaChiLienHe = diaChiLienHe;
		this.loaiHinhKinhDoanh = loaiHinhKinhDoanh;
		this.soDT = soDT;
		this.email = email;
		this.giayPhepKinhDoanh = giayPhepKinhDoanh;
		this.noiCap = noiCap;
		this.phuongThucDong = phuongThucDong;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getMaDonVi() {
		return maDonVi;
	}
	public void setMaDonVi(String maDonVi) {
		this.maDonVi = maDonVi;
	}
	public String getMaThue() {
		return maThue;
	}
	public void setMaThue(String maThue) {
		this.maThue = maThue;
	}
	public String getDiaChiKinhDoanh() {
		return diaChiKinhDoanh;
	}
	public void setDiaChiKinhDoanh(String diaChiKinhDoanh) {
		this.diaChiKinhDoanh = diaChiKinhDoanh;
	}
	public String getDiaChiLienHe() {
		return diaChiLienHe;
	}
	public void setDiaChiLienHe(String diaChiLienHe) {
		this.diaChiLienHe = diaChiLienHe;
	}
	public String getLoaiHinhKinhDoanh() {
		return loaiHinhKinhDoanh;
	}
	public void setLoaiHinhKinhDoanh(String loaiHinhKinhDoanh) {
		this.loaiHinhKinhDoanh = loaiHinhKinhDoanh;
	}
	public String getSoDT() {
		return soDT;
	}
	public void setSoDT(String soDT) {
		this.soDT = soDT;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGiayPhepKinhDoanh() {
		return giayPhepKinhDoanh;
	}
	public void setGiayPhepKinhDoanh(String giayPhepKinhDoanh) {
		this.giayPhepKinhDoanh = giayPhepKinhDoanh;
	}
	public String getNoiCap() {
		return noiCap;
	}
	public void setNoiCap(String noiCap) {
		this.noiCap = noiCap;
	}
	public String getPhuongThucDong() {
		return phuongThucDong;
	}
	public void setPhuongThucDong(String phuongThucDong) {
		this.phuongThucDong = phuongThucDong;
	}
	
	
	
}
