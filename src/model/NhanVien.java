package model;

import java.util.Date;

public class NhanVien {
	private String ten;
	private String gioiTinh;
	private String ngaySinh;
	private String diaChi;
	private String maBHXH;
	private String chucVu;
	private String mucluong;
	private String phuCap;
	private String noiKCB;
	public NhanVien() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NhanVien(String ten, String gioiTinh, String ngaySinh, String diaChi, String maBHXH, String chucVu,
			String mucluong, String phuCap, String noiKCB) {
		super();
		this.ten = ten;
		this.gioiTinh = gioiTinh;
		this.ngaySinh = ngaySinh; 
		this.diaChi = diaChi;
		this.maBHXH = maBHXH;
		this.chucVu = chucVu;
		this.mucluong = mucluong;
		this.phuCap = phuCap;
		this.noiKCB = noiKCB;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getMaBHXH() {
		return maBHXH;
	}
	public void setMaBHXH(String maBHXH) {
		this.maBHXH = maBHXH;
	}
	public String getChucVu() {
		return chucVu;
	}
	public void setChucVu(String chucVu) {
		this.chucVu = chucVu;
	}
	public String getMucluong() {
		return mucluong;
	}
	public void setMucluong(String mucluong) {
		this.mucluong = mucluong;
	}
	public String getPhuCap() {
		return phuCap;
	}
	public void setPhuCap(String phuCap) {
		this.phuCap = phuCap;
	}
	public String getNoiKCB() {
		return noiKCB;
	}
	public void setNoiKCB(String noiKCB) {
		this.noiKCB = noiKCB;
	}
	
	
}
