package model;

public class CaNhan {
	private String name;
	private String ngaySinh;
	private String gioiTinh;
	private String soCMND;
	private String diaChi;
	private String email;
	private String soDT;
	private String thanhPho;
	private String quan;
	private String maBHXH;
	private String noiKCB;
	public CaNhan() {
		super();
		// TODO Auto-generated constructor stub
	}
	public CaNhan(String name, String ngaySinh, String gioiTinh, String soCMND, String diaChi, String email,
			String soDT, String thanhPho, String quan, String maBHXH, String noiKCB) {
		super();
		this.name = name;
		this.ngaySinh = ngaySinh;
		this.gioiTinh = gioiTinh;
		this.soCMND = soCMND;
		this.diaChi = diaChi;
		this.email = email;
		this.soDT = soDT;
		this.thanhPho = thanhPho;
		this.quan = quan;
		this.maBHXH = maBHXH;
		this.noiKCB = noiKCB;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(String ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getSoCMND() {
		return soCMND;
	}
	public void setSoCMND(String soCMND) {
		this.soCMND = soCMND;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSoDT() {
		return soDT;
	}
	public void setSoDT(String soDT) {
		this.soDT = soDT;
	}
	public String getThanhPho() {
		return thanhPho;
	}
	public void setThanhPho(String thanhPho) {
		this.thanhPho = thanhPho;
	}
	public String getQuan() {
		return quan;
	}
	public void setQuan(String quan) {
		this.quan = quan;
	}
	public String getMaBHXH() {
		return maBHXH;
	}
	public void setMaBHXH(String maBHXH) {
		this.maBHXH = maBHXH;
	}
	public String getNoiKCB() {
		return noiKCB;
	}
	public void setNoiKCB(String noiKCB) {
		this.noiKCB = noiKCB;
	}
	
	
}
